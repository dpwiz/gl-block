{-# LANGUAGE DataKinds #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE DerivingStrategies #-}
{-# LANGUAGE DerivingVia #-}
{-# LANGUAGE ImportQualifiedPost #-}
{-# LANGUAGE TypeFamilies #-}

module Spec.Packed where

import Graphics.Gl.Block (Block, Packed(..))
import Graphics.Gl.Block qualified as Block

import GHC.Generics (Generic)
import Foreign (Storable)
import Foreign qualified
import Data.Proxy (Proxy(..))

import Test.Tasty.HUnit

data SomeStruct = SomeStruct
  { wat :: Bool -- 4
  , this :: Double -- 8
  , that :: Float -- 4
  , those :: (Float, Float) -- 4 + 4
  }
  deriving stock (Eq, Ord, Show, Generic)
  deriving anyclass Block
  deriving Storable via (Packed SomeStruct)

someStruct :: SomeStruct
someStruct = SomeStruct
  { wat = True
  , this = 2 * pi
  , that = pi
  , those = (1/60, -1e6)
  }

unit_packedSize :: (Block.PackedSize SomeStruct ~ 24) => Assertion
unit_packedSize = Block.sizeOfPacked (Proxy :: Proxy SomeStruct) @?= 24

unit_peekPoked :: Assertion
unit_peekPoked = do
  tripped <- Foreign.with someStruct Foreign.peek
  tripped @?= someStruct
