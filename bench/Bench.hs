{-# LANGUAGE ImportQualifiedPost #-}

import Test.Tasty.Bench (bench, bgroup, defaultMain, nf)

import Bench.Struct
import Data.Vector.Storable qualified as Storable

main :: IO ()
main = defaultMain
  [ bgroup "struct"
      [ bgroup "10"
          [ bench "manual" $
              nf (flip Storable.replicate structManual) 10
          , bench "packed" $
              nf (flip Storable.replicate structPacked) 10
          , bench "std140" $
              nf (flip Storable.replicate struct140) 10
          , bench "std430" $
              nf (flip Storable.replicate struct430) 10
          ]
      , bgroup "1000"
          [ bench "manual" $
              nf (flip Storable.replicate structManual) 1000
          , bench "packed" $
              nf (flip Storable.replicate structPacked) 1000
          , bench "std140" $
              nf (flip Storable.replicate struct140) 1000
          , bench "std430" $
              nf (flip Storable.replicate struct430) 1000
          ]
      , bgroup "1000000"
          [ bench "manual" $
              nf (flip Storable.replicate structManual) 1000000
          , bench "packed" $
              nf (flip Storable.replicate structPacked) 1000000
          , bench "std140" $
              nf (flip Storable.replicate struct140) 1000000
          , bench "std430" $
              nf (flip Storable.replicate struct430) 1000000
          ]
      ]
  ]
